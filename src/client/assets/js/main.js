let
  swReg

const
  SERVER_URL = 'http://127.0.0.1:5001'

  // Update UI for subscription status
  setSubscibedStatus = state => {
    const
      subscribeDiv = document.getElementById('subscribe'),
      unsubscribeDiv = document.getElementById('unsubscribe')

    if(state) {
      subscribeDiv.className = 'hidden'
      unsubscribeDiv.className = ''
    }
    else {
      subscribeDiv.className = ''
      unsubscribeDiv.className = 'hidden'
    }
  },

  // Get public key from server
  getApplicationServerKey = () => fetch(`${SERVER_URL}/key`)
    // Parse response as an arrayBuffer
    .then(res => res.arrayBuffer())
    // Convert arrayBuffer to Uint8Array
    .then(key => new Uint8Array(key))

  // Subscribe for push notifications
  subscribe = () => {
    if(!swReg) {
      console.error('Service Worker registration not found.')

      return
    }

    getApplicationServerKey()
      .then(applicationServerKey => {
        swReg.pushManager.subscribe({
            'userVisibleOnly': true,
            applicationServerKey
          })
          .then(subscription => {
            fetch(`${SERVER_URL}/subscribe`, {
              'method': 'POST',
              'body': JSON.stringify(subscription)
            })
            .then(setSubscibedStatus)
            .catch(unsubscribe)
          })
          .catch(console.error)
      })
  },

  // Unsubscribe from push **service**
  unsubscribe = () => {
    // Unsubscribe & update UI
    swReg.pushManager.getSubscription()
      .then(subscription => {
        subscription.unsubscribe()
          .then(() => setSubscibedStatus(false))
      })
  }


// Register Service Worker
if(navigator.serviceWorker) {
  navigator.serviceWorker.register('../../sw.js')
    .then(reg => {
      swReg = reg

      // Check for subscription status & update UI
      swReg.pushManager.getSubscription()
        .then(setSubscibedStatus)
    })
    .catch(console.error)
}

// UI event handlers
document.querySelector('#subscribe button').addEventListener('click', subscribe)
document.querySelector('#unsubscribe button').addEventListener('click', unsubscribe)
