SRC_DIR=src


all: http

.PHONY: http vapid.json

vapid.json:
	npx web-push generate-vapid-keys --json > ${SRC_DIR}/server/$@

http:
	-pkill caddy
	caddy
